FROM python:latest
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org flask
ENV NAME Bolenath
CMD ["python", "app.py"]
